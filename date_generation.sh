#!/bin/zsh
export j=1
export i=10
export k=1
for ((a=1; a>0; a++)) do
    printf $i
    
    if [[ $j -lt 10 ]]; then
        printf "0"$j
    else
        printf $j
    fi
    
    if [[ $k -lt 10 ]]; then
        printf "0"$k
        echo ""
    else
        printf $k
        echo ""
    fi
    if [ $k = 31 ]; then
        k=0
        if [ $j = 12 ]; then
            j=0
            (( i = $i + 1 ))      
        fi
        (( j = $j + 1 ))
        
        
        if [ $i = 16 ]; then
            break;
        fi
    fi
    (( k = $k + 1 ))
done
