#!/bin/bash

chrv() {
   local c
   printf -v c '\\%03o' "$1"
   printf -v "$2" "$c"
}

ansi_normal=$'\E[0m'
ansi_lightgreen=$'\E[92m'

while true; do
   printf '%s' "Encryption key (number from ${ansi_lightgreen}1$ansi_normal to ${ansi_lightgreen}26$ansi_normal): "
   read -re ekey
   if [[ $ekey = +([[:digit:]]) ]]; then
      ((ekey=10#$ekey))
      ((ekey>=1 && ekey<=26)) && break
   fi
   printf 'Bad number. Try again.\n'
done

# Build the lookup table according to this number
declare -A lookup

for i in {0..25}; do
   for u in 65 97; do
      chrv "$((i+u))" basechar
      chrv "$(((i+ekey-1)%26+u))" "lookup["$basechar"]"
   done
done

printf '%s' "Input (only ${ansi_lightgreen}letters$ansi_normal and ${ansi_lightgreen}spaces${ansi_normal}): "
IFS= read -re einput
read -n1 -rep "Do you want output to be uppercase? ${ansi_lightgreen}(y/n)$ansi_normal " oup
[[ ${oup,,} = y ]] && einput=${einput^^}

output=
linput=$einput
while [[ $linput ]]; do
   char=${linput::1}
   linput=${linput:1}
   if [[ ${lookup["$char"]} ]]; then
      # defined character
      output+=${lookup["$char"]}
   elif [[ $char = @(' '|$'\n') ]]; then
      # space and newline
      output+=$char
   else
      # other characters passed verbatim with warning message
      printf >&2 "Warning, character \`%s' not supported\n" "$char"
      output+=$char
   fi
done

printf 'Original text: %s\n' "$einput"
printf 'Encoded text: %s\n' "$output"
